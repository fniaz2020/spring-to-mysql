package com.farhadaniaz.SpringbootDevProject;

import com.farhadaniaz.SpringbootDevProject.dao.OrdersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringMySqlApplication {


	private static final Logger log = LoggerFactory.getLogger(SpringMySqlApplication.class);
	//Email sending must have its own class, u saved it in a notepad
	public static void main(String[] args) {

	//	SpringApplication.run(SpringMySqlApplication.class, args);
		SpringApplication.run(SpringMySqlApplication.class);
	}

	@Bean
	public CommandLineRunner demo(OrdersRepository repo) {
		return (args) -> {

			// fetch orders where id_raw ==0
			log.info("Order found with findById():");
			log.info("--------------------------------------------");
			Long i;  //For long, treat it like int but put L at end
			for(i=0L ; i<=6L; i++){
			repo.findByIdRaw(i).forEach(on -> {
				log.info(on.toString());
			});
			// for (Customer bauer : repository.findByLastName("Bauer")) {
			//  log.info(bauer.toString());
			// }
			log.info("");
		};
	}

}




