package com.farhadaniaz.SpringbootDevProject.api;

import com.farhadaniaz.SpringbootDevProject.dao.OrdersRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.farhadaniaz.SpringbootDevProject.model.Order;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.Date;


@RestController //<--Maybe try changing this to Controller
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersRepository ordersRepository;

    @PostMapping("/bookOrders") //Map ONLY POST Requests
    public @ResponseBody String bookOrder(@RequestParam Long idRaw , @RequestParam String orderId,
        @RequestParam String customer, @RequestParam String type, @RequestParam String shipTo, @RequestParam String shipToAddress,
        @RequestParam Date shipByDate, @RequestParam Date packByDate, @RequestParam Date inhouseDate,
        @RequestParam Date poDate, @RequestParam Date jobDate, @RequestParam String sourceFacility, @RequestParam String shipVia,
        @RequestParam String partNumber, @RequestParam Integer partQuantity,  @RequestParam String partDescription,
        @RequestParam String escalate,  @RequestParam String jobRequired,  @RequestParam String line2,  @RequestParam String responseAccept,
        @RequestParam Long epicorOrderId,  @RequestParam String jobNumber, @RequestParam Date dateCreated, @RequestParam Date dateUpdated,
        @RequestParam Date datePickedUp, @RequestParam Date dateCompleted, @RequestParam Date timeTaken, @RequestParam String auditStatus,
        @RequestParam String recipients, @RequestParam String subject, @RequestParam String body, @RequestParam byte[] attachements) {
      //Id is ur primary key so you dont include that here, thats automatic
        Order someorder = new Order();
        someorder.setIdRaw(idRaw);
        someorder.setOrderId(orderId);
        someorder.setCustomer(customer);
        someorder.setType(type);
        someorder.setShipTo(shipTo);
        someorder.setShipToAddress(shipToAddress);
        someorder.setShipByDate(shipByDate);
        someorder.setPackByDate(packByDate);
        someorder.setInhouseDate(inhouseDate);
        someorder.setPoDate(poDate);
        someorder.setJobDate(jobDate);
        someorder.setSourceFacility(sourceFacility);
        someorder.setShipVia(shipVia);
        someorder.setPartNumber(partNumber);
        someorder.setPartQuantity(partQuantity);
        someorder.setPartDescription(partDescription);
        someorder.setEscalate(escalate);
        someorder.setJobRequired(jobRequired);
        someorder.setLine2(line2);
        someorder.setResponseAccept(responseAccept);
        someorder.setEpicorOrderId(epicorOrderId);
        someorder.setJobNumber(jobNumber);
        someorder.setDateCreated(dateCreated);
        someorder.setDateUpdated(dateUpdated);
        someorder.setDatePickedUp(datePickedUp);
        someorder.setDateCompleted(dateCompleted);
        someorder.setTimeTaken(timeTaken);
        someorder.setAuditStatus(auditStatus);
        someorder.setRecipients(recipients);
        someorder.setSubject(subject);
        someorder.setBody(body);
        someorder.setAttachements(attachements);
        ordersRepository.save(someorder);
        return "Order has been booked";

    }

    @GetMapping("/all")  //U wanna get the WHOLE list/table
    public @ResponseBody Iterable<Order> getAllOrders() {
        //This returns a JSON or XML with the orders
        //Need to have another Get Mapping method with a
        // "Do while" loop in it.  That way u can tackle each row one by one and compose email
        return ordersRepository.findAll();
    }

  @GetMapping("/start")  //U wanna take each row 1 by 1 and compose email
  public @ResponseBody Iterable<Order> startProcessingOrders() {
    //"Do while" loop is here.  That way u can tackle each row one by one and compose email
    return ordersRepository.findAll();

  }
}

