package com.farhadaniaz.SpringbootDevProject.dao;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.farhadaniaz.SpringbootDevProject.model.Order;

public interface OrdersRepository extends CrudRepository<Order, Long> {

  List<Order> findByIdRaw (Long id);
}





