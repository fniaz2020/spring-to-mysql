package com.farhadaniaz.SpringbootDevProject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.GenerationType;
import org.springframework.web.bind.annotation.RequestParam;


//To specify a specific date format, u use SimpleDateFormat
//But if u make the date columns as type Date then u dont need simpledateformat
//Visit link https://dev.mysql.com/doc/ndbapi/en/mccj-using-clusterj-mappings.html
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id" )
    private Long id;     //U see that datatype of id is BigInt, so u gotta put Long instead
    @Column(name = "id_raw")
    private Long idRaw;
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "customer")
    private String customer;
    @Column(name = "type")
    private String type;
    @Column(name = "ship_to")
    private String shipTo;
    @Column(name = "ship_to_address")
    private String shipToAddress;
    @Column(name = "ship_by_date")
    Date shipByDate = new Date();
    @Column(name = "pack_by_date")
    Date packByDate = new Date();
    @Column(name = "inhouse_date")
    Date inhouseDate = new Date();
    @Column(name = "po_date")
    Date poDate = new Date();
    @Column(name = "job_date")
    Date jobDate = new Date();
    @Column(name = "source_facility")
    private String sourceFacility;
    @Column(name = "ship_via")
    private String shipVia;
    @Column(name = "part_number")
    private String partNumber;
    @Column(name = "part_quantity")
    private Integer partQuantity; //int -->Integer
    @Column(name = "part_description")
    private String partDescription;
    @Column(name = "escalate")
    private String escalate;
    @Column(name = "job_required")
    private String jobRequired;
    @Column(name = "line2")
    private String line2;
    @Column(name = "response_accept")
    private String responseAccept;
    @Column(name = "epicor_order_id")
    private Long epicorOrderId;
    @Column(name = "job_number")
    private String jobNumber;
    @Column(name = "date_created")
    Date dateCreated = new Date();
    @Column(name = "date_updated")
    Date dateUpdated = new Date();
    @Column(name = "date_picked_up")
    Date datePickedUp = new Date();
    @Column(name = "date_completed")
    Date dateCompleted = new Date();
    @Column(name = "time_taken")
    Date timeTaken = new Date();
    @Column(name = "retrieve_count")
    private Integer retrieveCount;
    @Column(name = "audit_status")
    private String auditStatus;
    @Column(name = "recipients")
    private String recipients;
    @Column(name = "subject")
    private String subject;
    @Column(name = "body")
    private String body;
    @Column(name = "attachements")
    private byte[] attachements;

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public byte[] getAttachements() {
        return attachements;
    }

    public void setAttachements (byte[] attachements) {
        this.attachements = attachements;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public Long getIdRaw() { return idRaw; }
    public void setIdRaw(Long id_raw) { this.idRaw = idRaw; }
    public String getOrderId() { return orderId; }
    public void setOrderId(String orderId) { this.orderId = orderId; }
    public String getCustomer() { return customer; }
    public void setCustomer(String customer) { this.customer = customer; }
    public String getType() { return type; }
    public void setType(String type) { this.type = type; }
    public String getShipTo() { return shipTo; }
    public void setShipTo(String shipTo) { this.shipTo = shipTo; }
    public String getShipToAddress() { return shipToAddress; }
    public void setShipToAddress(String shipToAddress) { this.shipToAddress = shipToAddress; }
    public Date getShipByDate() { return shipByDate; }
    public void setShipByDate(Date shipByDate) { this.shipByDate = shipByDate; }
    public Date getPackByDate() { return packByDate; }
    public void setPackByDate(Date packByDate) { this.packByDate = packByDate; }
    public Date getInhouseDate() { return inhouseDate; }
    public void setInhouseDate(Date inhouseDate) { this.inhouseDate = inhouseDate; }
    public Date getPoDate() { return poDate; }
    public void setPoDate(Date poDate) { this.poDate = poDate; }
    public Date getJobDate() { return jobDate; }
    public void setJobDate(Date jobDate) { this.jobDate = jobDate; }
    public String getSourceFacility() { return sourceFacility; }
    public void setSourceFacility(String sourceFacility) { this.sourceFacility = sourceFacility; }
    public String getShipVia() { return shipVia; }
    public void setShipVia(String shipVia) { this.shipVia = shipVia; }
    public String getPartNumber() { return partNumber; }
    public void setPartNumber(String partNumber) { this.partNumber = partNumber; }
    public Integer getPartQuantity() { return partQuantity; }
    public void setPartQuantity(int partQuantity) { this.partQuantity = partQuantity; }
    public String getPartDescription() { return partDescription; }
    public void setPartDescription(String partDescription) { this.partDescription = partDescription; }
    public String getEscalate() { return escalate; }
    public void setEscalate(String escalate) { this.escalate = escalate; }
    public String getJobRequired() { return jobRequired; }
    public void setJobRequired(String jobRequired) { this.jobRequired = jobRequired; }
    public String getLine2() { return line2; }
    public void setLine2(String line2) { this.line2 = line2; }
    public String getResponseAccept() { return responseAccept; }
    public void setResponseAccept(String responseAccept) { this.responseAccept = responseAccept; }
    public Long getEpicorOrderId() { return epicorOrderId; }
    public void setEpicorOrderId(Long epicorOrderId) { this.epicorOrderId = epicorOrderId; }
    public String getJobNumber() { return jobNumber; }
    public void setJobNumber(String jobNumber) { this.jobNumber = jobNumber; }
    public Date getDateCreated() { return dateCreated; }
    public void setDateCreated(Date dateCreated) { this.dateCreated = dateCreated; }
    public Date getDateUpdated() { return dateUpdated; }
    public void setDateUpdated(Date dateUpdated) { this.dateUpdated = dateUpdated; }
    public Date getDatePickedUp() { return datePickedUp; }
    public void setDatePickedUp(Date datePickedUp) { this.datePickedUp = datePickedUp; }
    public Date getDateCompleted() { return dateCompleted; }
    public void setDateCompleted(Date dateCompleted) { this.dateCompleted = dateCompleted; }
    public Date getTimeTaken() { return timeTaken; }
    public void setTimeTaken(Date timeTaken) { this.timeTaken = timeTaken; }
    public Integer getRetrieveCount() { return retrieveCount; }
    public void setRetrieveCount(int retrieveCount) { this.retrieveCount = retrieveCount; }
    public String getAuditStatus() { return auditStatus; }
    public void setAuditStatus(String auditStatus) { this.auditStatus = auditStatus; }
}
